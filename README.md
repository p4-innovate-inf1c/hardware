# Smart Platform

The smart platform has 5 main components:
1. ESP32
1. LDR sensor
1. Moisure senor
1. NFC Reader
1. CC2530 & CC2591

## ESP32
The esp will be used to power the platform and handle the communication of the NFC chip with CC2530 & cc2591

![ESP 32](./images/ESP32-Pinout.jpg)

## LDR sensor
To make the LDR sensor work we need:
- 5v/3.3v
- ground
- an analog input port

![LDR scheme](./images/schemes/LDR.png)

## Moisure sensor
To make the moisture sensor work we need:
- 5v/3.3v
- ground
- an analog input port

![Moisture senor](./images/schemes/Moisture.png)
## NFC Reader
No documentation yet

## Making firmware for the Webee CC2530 & CC2591
To flash the webee you need to have a Windows 10 ([download](https://www.microsoft.com/en-us/software-download/windows10ISO)) PC with latest PTVO software ([download](https://ptvo.info/download/ptvo-firmware-latest.zip))

### 1. Startup the software
![PTVO](./images/ptvo/start.png)

### 2. Select the board CC2530+CC2591
### 3. Select the outputs that have a data input

| PVTO                      | Description                                                                                   |
| ------------------------- | --------------------------------------------------------------------------------------------- |
| GPIO                      | General-purpose input/output is for digital input                                             |
| ADC (accurate. max 1.15V) | Analogue to Digital Converter is for analog inputs to max 1.15v                               |
| ADC (max 3.3V)            | Analogue to Digital Converter is for analog inputs to max 3.3v                                |
| ADC (raw value, max 3.3V) | Analogue to Digital Converter is for analog inputs to max 3.3v but leave the values untouched |

### 4. Save the hex file to your disk
Press the save button on the bottom of the screen to save a file to the disk

## Webee CC2530 & CC2591
To make this chip work you first need to flash it with a raspberry pi

### 1. Connect the Webee to the raspberry pi

Connect the wires using the scematics below.

***It is inportant that the cables are connected correctly or else the chip may no longer work!!***

|       | Zigbee module | Raspberry PI |
| ----- | ------------- | ------------ |
| VCC   | VCC           | 1            |
| GND   | GND           | 39           |
| Reset | RST           | 35           |
| DC    | P2.2          | 36           |
| DD    | P2.1          | 38           |

#### PINOUT
| Zigbee module                                               | Raspberry PI                         |
| ----------------------------------------------------------- | ------------------------------------ |
| ![Webee pinout](./images/webee_cc2530_cc2591_pinlayout.png) | ![pi_pinout](./images/pi-pinout.png) |

### 2. Make a bootable sd card with raspberry pi os (32 bit) using the Raspberry pi imager
for more information see https://www.raspberrypi.com/software/

### 3. Insert the SD card into the raspberry pi and run the installation

### 4. Enable ssh on the raspberry pi
for more information see https://phoenixnap.com/kb/enable-ssh-raspberry-pi

### 5. SSH into the pi from a remote computer
```shell
ssh pi@my-pi-ip
```
### 6. Become root
```shell
sudo su
```
### 7. Install dependencies
```shell
apt install git unzip
```
### 8. Install wiringPi
Follow the instructions on http://wiringpi.com/wiringpi-updated-to-2-52-for-the-raspberry-pi-4b/ to install wiringPi
### 9. Change to the opt directory
```shell
cd /opt
```
### 10. Clone the flash_cc2531 repo
```shell
git clone https://github.com/jmichault/flash_cc2531.git
```
### 11. Navigrate to the flash_cc2531 dir
```shell
cd flash_cc2531
```
### 12. Check if the device is connected correctly
if the output is `ffff` or `0000` the wiring is most likely not correct or the chip is broken.
```shell
./cc_chipid
```
### 13. Copy the firmware to the device
You can use different ways to copy the firmware to the device we will use for this demo scp
```shell
scp myfirmware.hex pi@my-pi-ip:/tmp/myfirmware.hex
```
### 14. Remove the content of the chip
```shell
./cc_erase
```
### 15. Flash the new firmware to the chip
```shell
./cc_write /tmp/myfirmware.hex
```

# ESP32 

## Dependencies
The ESP32 uses 2 libraries for the NFC readings, the two libraries are:
MFRC522.h 
SPI.h

In order to recieve the data from the NFC reader you need the SPI library The SPI command opens an bus to transfer the data. 
If a new bus is established, the MFRC522.h is used to read/write the NFC chip.

futhermore, you need the arduino IDE to use and/or create codee for the NFC reader.

## Code

if you need to replace the ESP32 you can upload the code from the branch NFC/nfc.ino

Open this file in the arduino IDE and flash the ESP with the provided code

