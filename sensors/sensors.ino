
const int RELAY_PIN = 7;

void setup() {
  pinMode(RELAY_PIN, OUTPUT); // relay
  Serial.begin(9600);
}

void loop() {
  /* LDR Sensor */
  int valueLDR = analogRead(A0);
  Serial.println(calculatePercentage(valueLDR, 1023)); // 1023 is max light

  /* Moisture meter */
  int valueMoisture = analogRead(A1);
  Serial.println(calculatePercentage(valueMoisture, 715)); // 715 is max moisture
  delay(200);

  /* Relay for watering */
  digitalWrite(RELAY_PIN, HIGH); // turn relay on
  delay(2000);
  digitalWrite(RELAY_PIN, LOW); // turn relay off
  delay(2000);
}

/* 
 *  
 * for the LDR sensor, value is the output from the photo-resistor
 * for the moisture, value is the output from the moisture meter
 *  
 */
float calculatePercentage(float value, float maxValue){
    float result = value / (maxValue / 100);
    return result;
}
